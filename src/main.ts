import dotenv from 'dotenv';
import {loadFile} from "./utils/load-csv";
import {mapTo, mergeMap, share, toArray} from "rxjs/operators";
import {filterByValid} from "./processing/filter";
import {normalize} from "./processing/normalize";
import {validate} from "./processing/validate";
import {Observable, zip} from "rxjs";
import {ValidatedEntry} from "@interfaces/raw-entry";

import {operationFailed, processBulkData} from "./process";
import {selectFile} from "./processing/select-file";
import {
  AporTypeValidator,
  IdTypeValidator,
  validators
} from "./validations/validators";
import {normalizers} from "./normalizers/normalizers";
import {
  decline,
  determineGrantStrategy
} from "./processing/granting-rules/grant-strategy";
import {filters} from "./filters/filters";

dotenv.config();

const startTime = new Date().getTime();

let select = selectFile();

const initializeAndLoadFiles$ = zip(AporTypeValidator.load(), IdTypeValidator.load()).pipe(
  mapTo(select)
);

const validatedEntries$: Observable<ValidatedEntry>  =
  initializeAndLoadFiles$
  .pipe(
    mergeMap(filePath => loadFile(filePath), 15),
    normalize(normalizers),
    filters(),
    validate(validators),
    share(),
  );

// Performs the insert operations on the database.
const valid$ = validatedEntries$.pipe(
  filterByValid(true),
  determineGrantStrategy(),
  toArray()
);

// Collects the erroneous entries.
const invalid$ = validatedEntries$.pipe(
  filterByValid(false),
  decline(),
  toArray()
);

zip(valid$, invalid$)
  .subscribe({
    next: ([successfulResults, invalidResults]) => processBulkData(startTime, successfulResults, invalidResults),
    error: operationFailed
  });

// psql.query("SELECT * FROM access_pass LIMIT 5", [], value => value).subscribe(value => {
//   console.dir(value);
//   psql.pool.end().then(() => console.log("closed pool connection"))
// }, err => {
//   console.error(err);
//   psql.pool.end().then(() => console.log("closed pool connection"))
// });
