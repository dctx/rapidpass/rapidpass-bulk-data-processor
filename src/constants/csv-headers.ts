/**
 * Ordered list of supported headers.
 */
export const headers : string[] = [
  'passType',
  'aporType',
  'firstName',
  'middleName',
  'lastName',
  'suffix',
  'company',
  'idType',
  'identifierNumber',
  'plateNumber',
  'mobileNumber',
  'email',
  'originName',
  'originStreet',
  'originCity',
  'originProvince',
  'destName',
  'destStreet',
  'destCity',
  'destProvince',
  'remarks'
];
