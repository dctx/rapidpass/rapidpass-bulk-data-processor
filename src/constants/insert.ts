import {ValidatedEntry} from "@interfaces/raw-entry";
import {log} from "../utils/logger";

const ACCESS_PASS_PARAMS = [
  'reference_id',
  'pass_type',
  'apor_type',
  'control_code',
  'id_type',
  'identifier_number',
  'name',
  'company',
  'plate_number',
  'remarks',
  'scope',
  'limitations',
  'origin_name',
  'origin_street',
  'origin_province',
  'origin_city',
  'destination_name',
  'destination_street',
  'destination_city',
  'destination_province',
  'valid_from',
  'valid_to',
  'issued_by',
  'updates',
  'status',
  'source',
  'date_time_created',
  'date_time_updated',
  'registrant_id'
];

const ACCESS_PASS_VALUES = ACCESS_PASS_PARAMS.map((val, index) => `$${index + 1}`).join(", ");

/**
 * Follows the format:
 *
 * ```
 * INSERT INTO access_passes (params, ...) VALUES ($1, $2...)
 * ```
 */
export const INSERT_ACCESS_PASS_STATEMENT = `INSERT INTO access_pass (${ACCESS_PASS_PARAMS}) VALUES (${ACCESS_PASS_VALUES})`;


/**
 * Returns the validated entries into the correct order for the insert statement.
 * @param entry
 * @param controlCode
 * @param registrantId
 * @param status
 * @param reasonForGrantOrDecline
 */
export function transformEntryToCreateAccessPassParams(entry: ValidatedEntry, controlCode: string, registrantId: number, status: string, reasonForGrantOrDecline: string): any[] {

  let referenceId = entry.mobileNumber || entry.plateNumber;

  if (entry.meta.passType === "INDIVIDUAL") {
    referenceId = entry.mobileNumber
  }

  if (entry.meta.passType === "VEHICLE") {
    referenceId = entry.plateNumber;
  }

  if (referenceId.length > 30) {
    log.error("Reference ID is not supposed to exceed length of 30 chars. Found " + referenceId);
  }

  if (entry.passType.length > 10) {
    log.error("Pass Type is not supposed to exceed length of 10 chars. Found " + entry.passType);
  }

  if (entry.aporType.length > 10) {
    log.error("Apor Type is not supposed to exceed length of 10 chars. Found " + entry.aporType);
  }

  const now = new Date();
  // Epoch Milli for April 30 midnight
  const april31Midnight = new Date(2020, 3, 30, 15, 59, 59);

  return [
    referenceId,
    entry.passType,
    entry.aporType,

    controlCode,
    entry.idType.substr(0, 10),
    entry.identifierNumber.substr(0, 25),
    entry.meta.name.substr(0, 100),
    entry.company.substr(0, 100),
    entry.plateNumber.substr(0, 20),
    entry.remarks.substr(0, 250),
    null,
    null,
    entry.originName.substr(0, 100),
    entry.originStreet.substr(0, 150),
    entry.originProvince.substr(0, 50),
    entry.originCity.substr(0, 50),
    entry.destName.substr(0, 100),
    entry.destStreet.substr(0, 150),
    entry.destCity.substr(0, 50),
    entry.destProvince.substr(0, 50),
    now,
    april31Midnight,
    'Darren bulk upload',
    reasonForGrantOrDecline.substr(0, 200),
    status,
    'BULK_DARREN',
    now,
    now,
    registrantId, // registrant id
  ];
}

