import {Filter} from "./filter";
import {RowsHasNonEmptyStringAtColumn} from "./RowsHasNonEmptyStringAtColumn";
import {filter} from 'rxjs/operators';
import {ValidatedEntry} from "@interfaces/raw-entry";

export const filterList : Filter[] = [
  new RowsHasNonEmptyStringAtColumn("passType"),
  new RowsHasNonEmptyStringAtColumn("aporType")
];

export const filters = () => filter((entry: ValidatedEntry) => {
  return filterList.reduce<boolean>((acc, curr) => acc && curr.filter(entry), true);
})
