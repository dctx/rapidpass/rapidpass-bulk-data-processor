import {Filter} from "./filter";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class RowsHasNonEmptyStringAtColumn implements Filter {

  private columnName: string;

  constructor(columnName: string) {
    this.columnName = columnName;
  }

  filter(entry: ValidatedEntry): boolean {
    return isNonNullAndUndefined(entry[this.columnName]) && entry[this.columnName] !== "";
  }
}
