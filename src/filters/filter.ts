import {ValidatedEntry} from "@interfaces/raw-entry";

export interface Filter {
  filter(entry: ValidatedEntry): boolean
}
