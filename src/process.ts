import {log} from "./utils/logger";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "./utils/object-utils";
import {psql} from "./database/PostgreSQLManager";

export const processBulkData = (startTime: number, successfulResults: any[], invalidResults: any[]) => {
  log.info("Insert database statements finished!");

  const endTime = new Date().getTime();

  const ms = endTime - startTime;
  const totalSeconds = ms / 1000;

  const inserts = successfulResults
    .map(v => v.meta.didCreate)
    .filter(isNonNullAndUndefined)
    .reduce((acc, curr) => acc + curr, 0);

  const grants = successfulResults
    .map(v => v.meta.didGrant)
    .filter(isNonNullAndUndefined)
    .reduce((acc, curr) => acc + curr, 0);

  const total = successfulResults.length;
  const invalidTotal = invalidResults.length;

  log.info(" --- SUMMARY --- ");
  log.info(`Successfully processed ${total + invalidTotal} total entries.`);
  log.info(`There were ${total} valid registrations and ${invalidTotal} invalid registrations.`);
  log.info(`Created ${inserts} new Access Passes.`);
  log.info(`Approved ${grants} existing pending Access Passes.`);
  log.info(`Created ${invalidTotal} declined Access Passes.`);
  log.info(`Processing took ${totalSeconds} seconds.`);

  log.info("Successful entries", successfulResults);

  const opsPerSec = (inserts + grants + invalidTotal) / totalSeconds;
  log.info(`Performance: ${opsPerSec.toFixed(2)} operations (insert/update) per second`);

  if (invalidResults.length > 0) {
    const invalidEntriesCount = invalidResults.length;
    log.error(" --- INVALID ERRORS --- ");
    log.error(`Found a total of ${invalidEntriesCount} invalid entries, which were not inserted because they had errors.`, invalidResults);
  }
  log.info("\n\n\n");

  psql.pool.end()
    .then(() => log.info("Successfully closed the pool connection."))
    .catch((error) => log.error("Failed to close the pool connection.", error))
};


export const validateBulkData = (startTime: number, invalidResults: any[]) => {
  log.info("CSV validation finished!");

  const endTime = new Date().getTime();

  const ms = endTime - startTime;
  const totalSeconds = ms / 1000;

  const invalidTotal = invalidResults.length;

  log.info(`Found a total of ${invalidTotal} invalid Access Passes.`);
  log.info(`Processing took ${totalSeconds} seconds.`);

  const opsPerSec = invalidTotal / totalSeconds;
  log.info(`Performance: ${opsPerSec.toFixed(2)} validated records per second`);

  if (invalidResults.length > 0) {
    const invalidEntriesCount = invalidResults.length;
    log.error(" --- INVALID ERRORS --- ");
    log.error(`These were the errors:`);
    invalidResults.forEach((value: ValidatedEntry) => {
      const errors = value.meta.errors.join(", ");
      log.error(`${value.meta.name} (${value.meta.referenceId}) - ${errors}`);
    })
  }

  log.info("\n\n\n");

  psql.pool.end()
    .then(() => log.info("Successfully closed the pool connection."))
    .catch((error) => log.error("Failed to close the pool connection.", error))
};

export function displayError(invalidatedEntry: ValidatedEntry[]){

}

export function operationFailed(error: any) {
  log.error("Failed to execute operation. ", error.attemptedEntry);
  log.error(error.message, error);
  psql.pool.end()
    .then(() => log.info("Successfully closed the pool connection."))
    .catch((error) => log.error("Failed to close the pool connection.", error))
}
