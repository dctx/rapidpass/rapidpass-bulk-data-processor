import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformReplace implements Normalizer {
  private _property: string;
  private find: string;
  private replace: string;

  constructor(property: string, find: string, replace: string) {
    this._property = property;
    this.find = find;
    this.replace = replace;
  }

  transform(entry: RawEntry): RawEntry {

    let value : string = entry[this._property];

    if (isNonNullAndUndefined(value) &&  (value.length > 0)) {
      value = value.replace(this.find, this.replace);
    }


    if (isNonNullAndUndefined(value)) {
      return {
        ...entry,
        [this._property]: value
      };
    }

    return { ...entry };
  }

  get name(): string { return `Replace ${this._property} - ${this.find} :=> ${this.replace}`};
}
