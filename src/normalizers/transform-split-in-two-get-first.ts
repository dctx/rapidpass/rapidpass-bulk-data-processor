import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformSplitInTwoGetFirst implements Normalizer {
  private _property: string;

  constructor(property: string) {
    this._property = property;
  }

  transform(entry: RawEntry): RawEntry {

    let value : string = entry[this._property] || "";

    const indexOfSlash = value.indexOf('/');

    if (indexOfSlash > 1)
      value = value.substr(0, indexOfSlash);

    const indexOfNewLine = value.indexOf('\n');

    if (indexOfNewLine > 1)
      value = value.substr(0, indexOfNewLine);

    const indexOfSemi = value.indexOf(';');

    if (indexOfSemi > 1)
      value = value.substr(0, indexOfSemi);

    if (isNonNullAndUndefined(value)) {
      return {
        ...entry,
        [this._property]: value.trim()
      };
    }

    return { ...entry };
  }

  get name(): string { return `Capitalize ${this._property}`};
}
