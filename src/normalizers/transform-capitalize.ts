import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformCapitalize implements Normalizer {
  private _property: string;

  constructor(property: string) {
    this._property = property;
  }

  transform(entry: RawEntry): RawEntry {

    const value : string = entry[this._property];

    if (isNonNullAndUndefined(value)) {
      return {
        ...entry,
        [this._property]: value.toUpperCase()
      };
    }

    return { ...entry };
  }

  get name(): string { return `Capitalize ${this._property}`};
}
