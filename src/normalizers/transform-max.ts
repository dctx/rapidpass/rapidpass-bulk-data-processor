import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformMax implements Normalizer {
  private _property: string;
  private count: number;

  constructor(property: string, count: number) {
    this._property = property;
    this.count = count;
  }

  transform(entry: RawEntry): RawEntry {

    let value : string = entry[this._property];

    if (isNonNullAndUndefined(value) &&  (value.length > 0)) {
      value = value.substr(0, this.count);
    }


    if (isNonNullAndUndefined(value)) {
      return {
        ...entry,
        [this._property]: value
      };
    }

    return { ...entry };
  }

  get name(): string { return `Max Characters ${this._property}`};
}
