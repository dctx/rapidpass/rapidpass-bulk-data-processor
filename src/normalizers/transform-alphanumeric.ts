import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformAlphanumeric implements Normalizer {
  private _property: string;

  constructor(property: string) {
    this._property = property;
  }

  transform(entry: RawEntry): RawEntry {

    let value : string = entry[this._property];

    if (isNonNullAndUndefined(value)) {
      value = value.replace(/[^A-Za-z0-9]+/g, "").toUpperCase();
    }

    if (isNonNullAndUndefined(value)) {
      return {
        ...entry,
        [this._property]: value
      };
    }

    return { ...entry };
  }

  get name(): string { return `Capitalize ${this._property}`};
}
