import {TransformMobileNumber} from "./transform-mobile-number";
import {TransformDefaultValue} from "./transform-default-value";
import {TransformPopulateMetadata} from "./transform-populate-metadata";
import {TransformTrim} from "./transform-trim";
import {TransformCapitalize} from "./transform-capitalize";
import {TransformSplitInTwoGetFirst} from "./transform-split-in-two-get-first";
import {TransformAlphanumeric} from "./transform-alphanumeric";
import {TransformMax} from "./transform-max";
import {TransformReplace} from "./transform-replace";

export const normalizers = [

  new TransformTrim("passType"),
  new TransformCapitalize("passType"),

  new TransformTrim("plateNumber"),
  new TransformCapitalize("plateNumber"),

  new TransformTrim("aporType"),
  new TransformCapitalize("aporType"),

  new TransformSplitInTwoGetFirst("email"),

  new TransformSplitInTwoGetFirst("mobileNumber"),
  new TransformAlphanumeric("mobileNumber"),
  new TransformReplace("mobileNumber", "O", "0"),
  new TransformMobileNumber(),
  new TransformTrim("mobileNumber"),


  new TransformTrim("company"),

  new TransformDefaultValue("email", ""),
  new TransformTrim("email"),
  new TransformDefaultValue("remarks", "frontliner"),

  new TransformDefaultValue("idType", "OTH"),
  new TransformTrim("idType"),

  new TransformSplitInTwoGetFirst("plateNumber"),
  new TransformAlphanumeric("plateNumber"),

  new TransformDefaultValue("identifierNumber", "OTH"),
  new TransformTrim("identifierNumber"),
  new TransformAlphanumeric("identifierNumber"),

  new TransformMax("aporType", 10),
  new TransformMax("identifierNumber", 25),
  new TransformMax("plateNumber", 20),
  new TransformMax("remarks", 200),
  new TransformMax("originStreet", 150),
  new TransformMax("originProvince", 50),
  new TransformMax("originCity", 50),


  new TransformMax("destStreet", 150),
  new TransformMax("destProvince", 50),
  new TransformMax("destCity", 50),

  new TransformPopulateMetadata()
];
