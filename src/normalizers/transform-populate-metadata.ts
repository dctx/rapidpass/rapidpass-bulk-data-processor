import {Normalizer} from "@interfaces/normalize";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformPopulateMetadata implements Normalizer {

  transform(entry: RawEntry): ValidatedEntry {

    const result : ValidatedEntry = {
      ...entry,
      meta: {
        errors: []
      }
    };


    result.meta.name = `${entry.firstName} ${entry.lastName}`;

    // Pass type
    if (isNonNullAndUndefined(entry.passType)) {
      if (entry.passType === "INDIVIDUAL") {
        result.meta.passType = "INDIVIDUAL";
        result.meta.referenceId = entry.mobileNumber;
      } else if (entry.passType === "VEHICLE") {
        result.meta.passType = "VEHICLE";
        result.meta.referenceId = entry.plateNumber;
      }
    }

      return result;
  }

  get name(): string { return `Populate metadata` };
}
