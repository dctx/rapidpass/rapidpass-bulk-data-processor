import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {isEmptyString, isNonNullAndUndefined} from "../utils/object-utils";
import {Observable, of} from "rxjs";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidateRequiredField implements ValidationRule {
  private _property: string;

  constructor(property: string) {
    this._property = property;
  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (!isNonNullAndUndefined(entry[this._property]) && !isEmptyString(this._property)) {
      return addErrorToValidatedEntry(entry, `Required field "${this._property}" is missing.`);
    }

    return duplicateValidatedEntry(entry);
  }

  validate(entry: ValidatedEntry): Observable<ValidatedEntry> {


    return null;
  }

}
