import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {from, Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {map, mapTo, mergeMap, tap, toArray} from "rxjs/operators";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";
import {log} from "../utils/logger";

export class ValidAporType implements ValidationRule {

  private aporValues: string[];

  constructor(private psql: PostgreSQLManager) {

  }

  /**
   * Initialization observable, to load the apor values onto this validator instance.
   */
  load() {
    log.debug("Loading APOR types.");
    return this.psql.query("SELECT * FROM lookup_table WHERE key IN ('APOR')", undefined, (value) => value.map(row => row.value))
      .pipe(
        mergeMap(entry => from(entry)),
        toArray(),
        tap((aporValues: string[]) => {
          log.debug(`Loaded the apor types successfully. Loaded a total of ${aporValues.length}.`);
          this.aporValues = aporValues;
        }, error => {
          log.error(`Failed to load APOR types.`, error);
        })
      );
  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    if (!this.aporValues.includes(entry.aporType)) {
      return addErrorToValidatedEntry(entry, 'Invalid Apor Type (' + entry.aporType + ').');
    }
    return duplicateValidatedEntry(entry);
  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
