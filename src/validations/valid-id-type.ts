import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {from, Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {map, mapTo, mergeMap, tap, toArray} from "rxjs/operators";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {log} from "../utils/logger";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidIdType implements ValidationRule {
  private idTypes: string[];
  constructor(private psql: PostgreSQLManager) {

  }

  load() {
    const sql = "SELECT * FROM lookup_table WHERE key IN ('IDTYPE-I', 'IDTYPE-V')";

    return this.psql.query(sql, undefined, (value) => value.map(v => v.value))
      .pipe(
        mergeMap(entry => from(entry)),
        toArray(),
        tap((idTypes: string[]) => {
          this.idTypes = idTypes;
        })
      );
  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (!this.idTypes.includes(entry.idType)) {

      const errorMessage = 'Invalid ID Type (idType=' + entry.idType + ').';

      return addErrorToValidatedEntry(entry, errorMessage);
    }
    return duplicateValidatedEntry(entry);

  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }

}
