import {ValidationRule} from "@interfaces/validator";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {Observable} from "rxjs";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidateMobileNumberCorrectLength implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (isNonNullAndUndefined(entry.passType) && entry.passType === 'INDIVIDUAL') {

      const mobileNumberExists = isNonNullAndUndefined(entry.mobileNumber);

      const length = mobileNumberExists ? entry.mobileNumber.length : 0;

      const exactLengthMatch = length === 11;

      // No errors.
      if (mobileNumberExists && exactLengthMatch)
        return duplicateValidatedEntry(entry);

      const errorMessage = 'Invalid mobile number length (len=' + length + '); expecting 09XXXXXXXXX format.';

      return addErrorToValidatedEntry(entry, errorMessage);
    } else {
      return entry;
    }
  }

  validate(entry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }

}
