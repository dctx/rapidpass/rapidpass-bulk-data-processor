import {ValidationRule} from "@interfaces/validator";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {Observable} from "rxjs";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidMobileNumberPattern implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (isNonNullAndUndefined(entry.passType) && entry.passType === 'INDIVIDUAL') {

      if (isNonNullAndUndefined(entry)) {
        const mobileNumber = entry.mobileNumber;

        const regex = RegExp("^09\\d{9}$");

        if (regex.test(mobileNumber)) {
          return duplicateValidatedEntry(entry);
        }
      }

      const errorMessage = "Invalid mobile number pattern. It should start with 09 and should be 11 characters (found len=" + entry.mobileNumber.length + ").";

      return addErrorToValidatedEntry(entry, errorMessage);

    }
    return entry;
  }

  validate(entry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
