import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {from, Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {map, mapTo, mergeMap, tap, toArray} from "rxjs/operators";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidHasNoExistingApprovedOrPendingAccessPass implements ValidationRule {

  constructor(private psql: PostgreSQLManager) {

  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    return null;
  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {

    const sql = "SELECT * FROM access_pass WHERE reference_id = $1 ORDER BY valid_to DESC";

    const errorMessage = 'An existing PENDING/APPROVED RapidPass already exists for ' + validatedEntry.meta.referenceId + ').';

    return this.psql.query(sql, [validatedEntry.meta.referenceId], (value) => value)
      .pipe(
        mergeMap(entry => from(entry)),
        toArray(),
        map((existingAccessPasses: string[]) => {

          if (existingAccessPasses.length > 0) {
            return addErrorToValidatedEntry(validatedEntry, errorMessage);
          }

          return duplicateValidatedEntry(validatedEntry);
        })
      );
  }
}
