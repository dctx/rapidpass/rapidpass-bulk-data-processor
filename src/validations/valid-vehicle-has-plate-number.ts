import {ValidationRule} from "../interfaces/validator";
import {RawEntry, ValidatedEntry} from "../interfaces/raw-entry";
import {Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {isEmptyString} from "../utils/object-utils";
import {error} from "winston";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidVehicleHasPlateNumber implements ValidationRule {

  constructor() {

  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    if (entry.meta.passType === "VEHICLE" && isEmptyString(entry.plateNumber)) {
      const errorMessage = 'Plate number is required for vehicle passes.';
      return addErrorToValidatedEntry(entry, errorMessage);
    }

    return duplicateValidatedEntry(entry);
  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
