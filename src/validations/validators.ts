import {ValidMobileNumberPattern} from "./valid-mobile-number-pattern";
import {ValidateRequiredField} from "./validate-required-field";
import {ValidPassType} from "./valid-pass-type";
import {ValidVehicleHasPlateNumber} from "./valid-vehicle-has-plate-number";
import {ValidAporType} from "./valid-apor-type";
import {psql} from "../database/PostgreSQLManager";
import {ValidIdType} from "./valid-id-type";

export const AporTypeValidator = new ValidAporType(psql);

export const IdTypeValidator = new ValidIdType(psql);

export const validators = [
  new ValidateRequiredField("passType"),
  new ValidateRequiredField("aporType"),

  new ValidateRequiredField("mobileNumber"),
  new ValidateRequiredField("firstName"),
  new ValidateRequiredField("lastName"),

  new ValidMobileNumberPattern(),

  new ValidPassType(),
  AporTypeValidator,
  // IdTypeValidator,
  new ValidVehicleHasPlateNumber(),
];
