import {ValidationRule} from "@interfaces/validator";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {Observable} from "rxjs";
import {
  addErrorToValidatedEntry,
  duplicateValidatedEntry
} from "../utils/add-error";

export class ValidPassType implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (!["INDIVIDUAL", "VEHICLE"].includes(entry.passType)) {
      const errorMessage = 'Invalid Pass Type (passType=' + entry.passType + ').';
      return addErrorToValidatedEntry(entry, errorMessage);
    }

    return duplicateValidatedEntry(entry);
  }


  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
