import {psql} from "../PostgreSQLManager";
import {ValidatedEntry} from "@interfaces/raw-entry";

const REGISTRANT_PARAMS : string[] = [
  'registrant_type',
  'registrant_name',
  'first_name',
  'middle_name',
  'last_name',
  'suffix',
  'email',
  'mobile',
  'reference_id_type',
  'reference_id',
  'registrar_id'
];

const REGISTRANT_VALUES = REGISTRANT_PARAMS.map((val, index) => `$${index + 1}`).join(", ");

export const INSERT_REGISTRANT_STATEMENT = `INSERT INTO registrant (${REGISTRANT_PARAMS}) VALUES (${REGISTRANT_VALUES})`;

export function InsertRegistrant(entry: ValidatedEntry) {
  const query = INSERT_REGISTRANT_STATEMENT;

  const params = TransformRegistrantParams(entry);

  return psql.query(query, params, (value) => value)
}

export function TransformRegistrantParams(entry: ValidatedEntry): any[] {
  return [
    1,
    entry.meta.name.substr(0, 50),
    entry.firstName.substr(0, 50),
    entry.middleName.substr(0, 50),
    entry.lastName.substr(0, 50),
    entry.suffix.substr(0, 10),
    entry.email.substr(0, 50),
    entry.mobileNumber.substr(0, 50),
    entry.idType.substr(0, 20),
    entry.identifierNumber.substr(0, 50),
    0
  ]

}
