import {Observable} from "rxjs";
import {psql} from "../PostgreSQLManager";

export function SelectRegistrantIdsByMobile(mobileNumber: string): Observable<number[]> {
  return psql.query("SELECT id from registrant WHERE mobile = $1", [mobileNumber], (value) => value.map(v => v.id));
}
