import Pool from "pg-pool";
import {Client, PoolClient} from "pg";
import {Observable} from "rxjs";
import dotenv from 'dotenv';
import {mergeMap} from "rxjs/operators";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {AccessPass} from "@interfaces/AccessPass";

dotenv.config();

interface PsqlPoolHandler {
  client: PoolClient;
  release: Function
}

export class PostgreSQLManager {
  private _pool: Pool<Client>;

  get pool(): Pool<Client> {
    return this._pool;
  }

  public constructor() {

    const psqlConfig = {
      host: process.env.POSTGRESQL_HOST,
      database: process.env.POSTGRESQL_DB,
      user: process.env.POSTGRESQL_USER,
      password: process.env.POSTGRESQL_PASS,
      port: Number(process.env.POSTGRESQL_PORT),
      max: Number(process.env.POSTGRESQL_MAX_POOL),
      idleTimeoutMillis: Number(process.env.POSTGRESQL_CONN_TIMEOUT)
    };

    this._pool = new Pool(psqlConfig);
  }

  connect() {
    return new Observable<PsqlPoolHandler>(subscriber => {
      this.pool.connect(((err, client, done) => {
        if (err) {
          subscriber.error(err);
          return;
        }

        subscriber.next({
          client: client,
          release: done
        } as PsqlPoolHandler);
        subscriber.complete();

      }));
    });
  }

  /**
   * Active access pass are retrieved using the following criterias:
   *
   * 1. First, we filter access passes by the associated person, distinguished
   * by their referenceId.
   *
   * 2. Second, we filter all access passes by their status: only approved
   * or pending access passes make sense to be active because of negation:
   * Suspended and Declined passes are not active.
   *
   * 2. Third, a person's most active pass is the one who has the latest
   * valid_to. This is an assumption that we can make (without computing what
   * their valid_from and valid_to) because we grant the validity beginning
   * from the moment of granting up to the end of April.
   *
   * These criteria allow us to determine:
   *
   * - If there are no results (ether no passes at all, or either declined
   * or suspended) - a new pass must be created.
   *
   * - If there is a result, it is the 'most active pass'.
   *
   *    a. If the timestamp right now is before the valid_to, then the pass is
   *       still active. We'll do nothing, since they already have a pass.
   *
   *    b. If the timestamp right now is beyond the valid_to, then their pass
   *       has already expired. We'll create a new pass for them.
   *
   * @param referenceId
   */
  findAllActiveAccessPassByReferenceId(referenceId: string): Observable<AccessPass[]> {
    const sql = "SELECT * FROM access_pass WHERE reference_id = $1 AND status IN ('APPROVED', 'PENDING') ORDER BY valid_to DESC LIMIT 1";
    const params = [referenceId];

    return this.query(sql, params, (value => value));
  }

  query<E extends any>(queryStatement: string, parameters: any[], transformer: (value: any[]) => E[]): Observable<E[]> {
    return this.connect()
      .pipe(
        mergeMap((poolHandler: PsqlPoolHandler) => this.runQuery(poolHandler, queryStatement, parameters, transformer))
      )
  }

  runQuery(client: PsqlPoolHandler, query: string, parameters: any[], transformer: (value: any[]) => any) {

    return new Observable<any[]>(subscriber => {

      client.client.query(query, parameters, (err, res) => {
        if (err) {
          subscriber.error(err);
          return;
        }
        //
        // client = null;
        // parameters = null;
        // query = null;
        //

        client.release();

        if (isNonNullAndUndefined(transformer)) {
          subscriber.next(transformer(res.rows));
        } else {
          subscriber.next(null);
        }
        subscriber.complete();
      });

    });

  }


}


function exitHandler(options, exitCode) {
  // if (options.cleanup) console.log('cleaning up.');
  // if (exitCode || exitCode === 0) console.log(exitCode);

  psql.pool.end().then(() => {});

  if (options.exit) process.exit();
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));


export const psql = new PostgreSQLManager();
