import {Observable, throwError} from "rxjs";
import {psql} from "../../database/PostgreSQLManager";
import {AccessPass} from "@interfaces/AccessPass";
import {catchError, map, mapTo, mergeMap, tap} from "rxjs/operators";
import {GrantExistingAccessPasses} from "./grant-existing-access-passes";
import {ApprovedCreateOnlyIfExpired} from "./create-if-expired";
import {CreateNewAccessPass} from "./create";
import {isNonNullAndUndefined} from "../../utils/object-utils";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {log} from "../../utils/logger";


export function determineGrantStrategy() {
  return mergeMap((entry: ValidatedEntry) => {

    let accessPass$ = getExistingAccessPass(entry.meta.referenceId);

    return accessPass$.pipe(
      mergeMap(accessPass => determineOperation(entry, accessPass)),
      mapTo(entry),
      catchError(error => {
        error.attemptedEntry = entry;
        return throwError(error)
      })
    );
  })
}

/**
 * Returns the existing active access pass.
 * @param referenceId
 * @return an access pass or null.
 */
export function getExistingAccessPass(referenceId: string): Observable<AccessPass> {

  return psql.findAllActiveAccessPassByReferenceId(referenceId).pipe(
    map(results => {
      if (Array.isArray(results) && results.length > 0)
        return results[0];
      else
        return null;
    })
  );
}

export function decline() {
  return mergeMap<ValidatedEntry, Observable<any>>(entry => {
    log.warn(`Failed validation for ${entry.meta.referenceId}: ${entry.meta.errors.join(" ")}`);
    return CreateNewAccessPass(entry, "DECLINED")
      .pipe(mapTo(entry));
  });
}

export function determineOperation(validatedEntry: ValidatedEntry, accessPass: AccessPass): Observable<any> {

  if (!isNonNullAndUndefined(accessPass)) {
    log.info("No pass- creating a new access pass.");
    return CreateNewAccessPass(validatedEntry, "APPROVED");
  }

  switch (accessPass.status) {
    case "PENDING":
      log.info("Existing pending pass (refId=" + accessPass.reference_id +")- granting it.");
      return GrantExistingAccessPasses(accessPass.id).pipe(
        tap(() => validatedEntry.meta.didGrant = true)
      );
    case "APPROVED":
      // double check if valid, if yes- skip. if no- create.
      return ApprovedCreateOnlyIfExpired(validatedEntry, accessPass);
  }

  return throwError(new Error("Invalid access pass status: " + accessPass.status));
}
