import {ValidatedEntry} from "@interfaces/raw-entry";
import {SelectRegistrantIdsByMobile} from "../../database/queries/select-registrant-by-mobile";
import {InsertRegistrant} from "../../database/queries/insert-registrant";
import {map, mapTo, mergeMap, mergeMapTo, tap} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {
  INSERT_ACCESS_PASS_STATEMENT,
  transformEntryToCreateAccessPassParams
} from "../../constants/insert";
import {psql} from "../../database/PostgreSQLManager";
import {log} from "../../utils/logger";

export function CreateAccessPass(entry: ValidatedEntry, registrantId: number, status: string, reasonForGrantOrDecline: string) {

  log.info("Creating new access pass for registrant_id=" + registrantId, entry);

  const query = INSERT_ACCESS_PASS_STATEMENT;

  const params = transformEntryToCreateAccessPassParams(entry, null, registrantId, status, reasonForGrantOrDecline);

  const updateParams = (params: any[], registrantId: number) => {
    params[params.length - 1] = registrantId;
    return params;
  };

  const updatedParameters = updateParams(params, registrantId);

  return psql.query(query, updatedParameters, (value) => value.map(v => v.rows));
}

export function CreateNewAccessPass(entry: ValidatedEntry, status: string): Observable<string> {

  const selectRegistrant$ = SelectRegistrantIdsByMobile(entry.mobileNumber);

  const insertRegistrant$ = InsertRegistrant(entry);

  const retrieveRegistrantId$ = selectRegistrant$.pipe(
    mergeMap((ids: number[]) => {

      if (ids[0]) return of(ids[0]);

      return insertRegistrant$.pipe(
        mergeMapTo(selectRegistrant$),
        map(result => result[0])
      );
    })
  );

  return retrieveRegistrantId$
    .pipe(
      mergeMap((registrantId: number) => {
        const reasonForGrantOrDecline = entry.meta.errors.join(" ").substr(0, 200);
        return CreateAccessPass(entry, registrantId, status, reasonForGrantOrDecline);
      }),
      tap(() => entry.meta.didCreate = true),
      mapTo(entry.meta.referenceId)
    );
}
