import {Observable, of} from "rxjs";
import {AccessPass} from "@interfaces/AccessPass";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {CreateNewAccessPass} from "./create";
import {mapTo, tap} from "rxjs/operators";
import {log} from "../../utils/logger";

/**
 * Creates a new access pass only if the existing access pass is expired.
 * @constructor
 * @param validatedEntry
 * @param existingAccessPass
 */
export function ApprovedCreateOnlyIfExpired(validatedEntry: ValidatedEntry, existingAccessPass: AccessPass): Observable<string> {

  let validTo = existingAccessPass.valid_to;

  let date = new Date();

  const isExpired = date.getTime() > validTo.getTime();

  if (isExpired) {
    log.info("Existing approved pass but expired, creating new access pass.");
    return CreateNewAccessPass(validatedEntry, "APPROVED").pipe(
      tap(() => validatedEntry.meta.didCreate = true)
    );
  }

  // log.info("Existing approved pass but still active, skipping.");
  return of(existingAccessPass).pipe(mapTo(existingAccessPass.reference_id));
}
