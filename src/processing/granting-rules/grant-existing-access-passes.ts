import {psql} from "../../database/PostgreSQLManager";

/**
 * Performs a grant operation on the existing access pass, updating its status.
 *
 * Note that the `id` provided maps to `AccessPass.id`.
 * @param id
 * @constructor
 */
export function GrantExistingAccessPasses(id: number) {

  const now = new Date().toISOString();

  // Epoch Milli for April 30 midnight
  const april30Midnight = new Date(2020, 3, 30, 23, 59, 59).toISOString();

  const query = "UPDATE access_pass SET status = $1, source = $2, valid_to = $3, date_time_updated = $4 WHERE id = $5";
  const params = [
    'APPROVED',
    'BULK_DARREN',
    april30Midnight,
    now,
    id
  ];

  return psql.query(query, params, (value => value));
}
