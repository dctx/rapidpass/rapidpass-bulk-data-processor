import {filter} from "rxjs/operators";
import {ValidatedEntry} from "@interfaces/raw-entry";

export function filterByValid(isValid: boolean){
  return filter((e: ValidatedEntry) => {
    return e.meta.isValid === isValid;
  });
}
