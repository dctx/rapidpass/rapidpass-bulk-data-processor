import {map} from "rxjs/operators";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {Normalizer} from "@interfaces/normalize";

export function normalize(transformers: Normalizer[]){
  return  map<RawEntry, ValidatedEntry>((value: RawEntry) => {
    return transformers.reduce((acc, transformer) => transformer.transform(acc), value) as ValidatedEntry;
  });
}
