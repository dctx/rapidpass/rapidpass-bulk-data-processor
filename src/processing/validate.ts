import {concatMap, map, mapTo, mergeMap, tap} from "rxjs/operators";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {ValidationRule} from "@interfaces/validator";
import {from, Observable, of} from "rxjs";

export function validate(validationRules: ValidationRule[]){

  return map<ValidatedEntry, ValidatedEntry>((value: ValidatedEntry) => {

    const result = validationRules.reduce((acc, curr) => curr.validateNonRx(acc), value);

    result.meta.isValid = result.meta.errors.length === 0;

    return result;

  });
}
