import {ValidatedEntry} from "@interfaces/raw-entry";

export type Result = [ValidatedEntry[], ValidatedEntry[]];
