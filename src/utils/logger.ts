import winston, {format} from 'winston';
import DailyRotateFile from "winston-daily-rotate-file";

export const log = winston.createLogger({
  level: 'info',
  format: format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.colorize()
  ),
  defaultMeta: undefined, // { service: 'rapidpass-bulk-data-processor-service' },
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new DailyRotateFile({
      filename: 'combined-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: false,
      maxSize: '20m',
      maxFiles: '14d',
      level: 'info',
      dirname: 'logs'
    }),
    new DailyRotateFile({
      filename: 'errors-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: false,
      maxSize: '20m',
      maxFiles: '14d',
      level: 'error',
      dirname: 'logs'
    })
  ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  log.add(new winston.transports.Console({
    level: 'warn',
    format: winston.format.simple()
  }));
}
