import {ValidatedEntry} from "@interfaces/raw-entry";

/**
 * Returns a new object.
 * @param validatedEntry
 * @param errorMessage
 */
export function addErrorToValidatedEntry(validatedEntry: ValidatedEntry, errorMessage: string): ValidatedEntry {
  let newValidatedEntry = duplicateValidatedEntry(validatedEntry);

  newValidatedEntry.meta.errors.push(errorMessage);

  return newValidatedEntry;
}

export function duplicateValidatedEntry(validatedEntry: ValidatedEntry): ValidatedEntry {
  return {
    ...validatedEntry,
    meta: {
      ...validatedEntry.meta,
      errors:  validatedEntry.meta.errors.concat()
    }
  } as ValidatedEntry;
}
