import csv from 'csv-parser';
import fs from 'fs';
import {Observable, Subject} from "rxjs";
import {RawEntry} from "@interfaces/raw-entry";
import {headers} from "../constants/csv-headers";
import {Transform} from 'stream';

export const subject = new Subject<boolean>();

export function loadFile(filePath: string): Observable<RawEntry> {
    return new Observable(subscriber => {
        const readStream : Transform = fs.createReadStream(filePath)
          .pipe(csv({
            headers: headers,
            skipLines: 1,
            strict: true
          }))
          .on('data', (data) => {
            subscriber.next(data);
          })
          .on('end', () => {
              subscriber.complete();
          })
          .on('error', (err) => {
              subscriber.error(err);
        });
    })
}
