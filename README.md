## Installation

1. `npm install`
2. Configure `.env` file.

## Usage

1. `npm run clean` - To clean `input` folder.
2. `npm run split` - To cut up large csv file into files that are 7k lines each.
3. `npm run start` - To begin processing data on the target file.
